# 2020_assignment3_videoteca

Terzo Assignemnt del corso "progetto e sviluppo Software".

### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587 @M-Marchi
- Stoppa Miguel 820506 @mig1214
- Tomasoni Lorenzo 829906 @lTomasoni


### Videoteca

“Videoteca” è una semplice applicazione web che consente di gestire l’acquisto di film da parte degli utenti iscritti, dando la possibilità ad essi di visualizzare la lista di film comprati e di acquistarne di nuovi. È inoltre possibile visualizzare le informazioni di ogni film, come ad esempio: il genere, il regista, l’anno di pubblicazione, il cast o l’eventuale presenza di un sequel. 



Tutte le informazioni riguardanti la realizzazione dell'assingment e l'installazione saranno reperibili nella cartella "Document".


