package com.videoteca.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.videoteca.model.Admin;
import com.videoteca.model.Copy;
import com.videoteca.model.Customer;
import com.videoteca.model.Movie;
import com.videoteca.servicies.AdminService;
import com.videoteca.servicies.CopyService;
import com.videoteca.servicies.CustomerService;
import com.videoteca.servicies.MovieService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminServiceTests {

	@Autowired
	private AdminService adminService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private MovieService movieService;
	@Autowired
	private CopyService copyService;
	
	
	/**
	 * Controlliamo che gli admin siano presenti effettivamente nel DB
	 * controllando anche elementi non presentinel DB
	 * */
	@Test
	public void checkAdmins() {
		
		Admin admin = adminService.GetAdmin("john.doe@outlook.com");
		Admin admin2 = adminService.GetAdmin("a.rossi@gmail.com");
		Admin admin3 = adminService.GetAdmin("assignment3@gmail.com");
		
		assertNotNull(admin);
		assertNotNull(admin2);
		assertNull(admin3);
		
		assertEquals("john.doe@outlook.com", admin.getEmail());
		assertEquals("a.rossi@gmail.com", admin2.getEmail());
	}
	
	
	/**
	 * Controlliamo che i customer siano presenti effettivamente nel DB
	 * controllando anche elementi non presentinel DB
	 * */
	@Test
	public void checkCustomers() {
		
		Customer customer = customerService.getCustomer("mario.rossi@gmail.com");
		Customer customer2 = customerService.getCustomer("giovanni.verdi@gmail.com");
		Customer customer3 = customerService.getCustomer("federico.bianchi@outlook.com");
		Customer customer4 = customerService.getCustomer("fio.marino@outlook.com");
		Customer customer5 = customerService.getCustomer("degna.davide@outlook.com");
		Customer customer6 = customerService.getCustomer("assignment3@outlook.com");
		
		assertNotNull(customer);
		assertNotNull(customer2);
		assertNotNull(customer3);
		assertNotNull(customer4);
		assertNotNull(customer5);
		assertNull(customer6);
		
		assertEquals("mario.rossi@gmail.com", customer.getEmail());
		assertEquals("giovanni.verdi@gmail.com", customer2.getEmail());
		assertEquals("federico.bianchi@outlook.com", customer3.getEmail());
		assertEquals("fio.marino@outlook.com", customer4.getEmail());
		assertEquals("degna.davide@outlook.com", customer5.getEmail());
		
	}
	
	/**
	 * Verifichiamo l'aggiunta di un nuovo film
	 * */
	@Test
	public void addMovie() {
		
		Movie movie = new Movie();
		
		movie.setMovie_title("Ready Player One");
		movie.setGenre("Sci-fi");
		movie.setDirector("Steven Spielberg");
		movie.setYear(2018);
		
		movieService.addNewMovie(movie.getMovie_title(), movie.getGenre(),
				movie.getDirector(), movie.getYear());
		
		Movie memMovie = movieService.getByTitle("Ready Player One");
		
		assertEquals("Ready Player One", memMovie.getMovie_title());
	}
	
	/**
	 * Verifichiamo di poter associare una copia di un film a un cliente
	 * */
	
	@Test
	public void buyCopy() {
		
		Iterable<Copy> copies = copyService.getCopiesByCustomer("federico.bianchi@outlook.com");
		boolean exist = false;
		for (Copy copy : copies) {
			if(copy.getMovie().getMovie_title().equals("The martian"))
				exist = true;
		}
		
		assertFalse(exist); // Ci aspettiamo che ci dia falso poichè la copia non è ancora associata
		
		copyService.buyCopy("federico.bianchi@outlook.com",39);	//Associamo la copia al cliente
		
		copies = copyService.getCopiesByCustomer("federico.bianchi@outlook.com");
		exist = false;
		for (Copy copy : copies) {
			if(copy.getMovie().getMovie_title().equals("The martian"))
				exist = true;
		}
		
		
		assertTrue(exist); // Il valore ora è vero poichè abbiamo associato la copia al cliente
	}
	

}
