package com.videoteca.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
//@DiscriminatorValue("ADMIN")
public class Admin extends User {
	
	private String contract;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "Manage", 
			joinColumns = { @JoinColumn(name = "email") }, 
	        inverseJoinColumns = { @JoinColumn(name = "copy_id") })
	private Set<Copy> copies = new HashSet<>();
	
	public Admin() {
		super(null, null, null);
	}
	
	public Admin(String email, String nickname, String password, String contract) {
		super(email, nickname, password);
		
		
		this.contract = contract;
	}
	
	
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	
	@Override
	public String toString() {
		return "Admin [contract=" + contract  + "]";
	}

	public Set<Copy> getCopies() {
		return copies;
	}

	public void setCopies(Set<Copy> copies) {
		this.copies = copies;
	}
	
	
	
	
}
