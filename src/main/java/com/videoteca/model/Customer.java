package com.videoteca.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
//@DiscriminatorValue("CUSTOMER")
public class Customer extends User {

	private Date registration;
	
	// Relationship 1:N with Copy, n side
	@OneToMany(mappedBy="copy_id", cascade = CascadeType.ALL)
	private Set<Copy> copies;
	
	public Customer() {
		super(null, null, null);
	}
	
	public Customer(String email, String nickname, String password, Date registration) {
		super(email, nickname, password);
		
		this.registration = registration;
	}

	public Date getRegistration() {
		return registration;
	}

	public void setRegistration(Date registration) {
		this.registration = registration;
	}

	@Override
	public String toString() {
		return "Customer [registration=" + registration + ", getEmail()=" + getEmail() + ", getNickname()="
				+ getNickname() + ", getPassword()=" + getPassword() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	

}
