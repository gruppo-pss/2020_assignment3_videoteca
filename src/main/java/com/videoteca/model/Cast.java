package com.videoteca.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Cast {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long cast_id;
	
	private String name;
	private String surname;
	private Date birthdate;
	
	 @ManyToMany(fetch = FetchType.LAZY,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	    @JoinTable(name = "Act",
	            joinColumns = { @JoinColumn(name = "cast_id") },
	            inverseJoinColumns = { @JoinColumn(name = "movie_title") })
	    private Set<Movie> Movie = new HashSet<>();
	 
	 
		
	public Cast() {
		
	}
	
	public Cast(String name, String surname,  Date birthdate) {
		
		this.name = name;
		this.surname = surname;
		this.birthdate = birthdate;
	}
	
	public long getCast_id() {
		return cast_id;
	}
	
	public void setCast_id(long cast_id) {
		this.cast_id = cast_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Date getBirthdate() {
		return birthdate;
	}
	
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	
	
	@Override
	public String toString() {
		return "Cast [cast_id=" + cast_id + ", name=" + name + ", surname=" + surname + ", birthdate=" + birthdate
				+ "]";
	}

	public Set<Movie> getMovie() {
		return Movie;
	}

	public void setMovie(Set<Movie> movie) {
		Movie = movie;
	}
	
	

}
