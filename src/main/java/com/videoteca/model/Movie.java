package com.videoteca.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Movie {

	@Id
	private String movie_title;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "sequel", referencedColumnName = "movie_title")
	private Movie sequel;
	
	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	private Set<Copy>copies;
	
	@ManyToMany(fetch = FetchType.LAZY,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	@JoinTable(name = "Act",
	            joinColumns = { @JoinColumn(name = "movie_title") },
	            inverseJoinColumns = { @JoinColumn(name = "cast_id") })
	private Set<Cast> cast = new HashSet<>();
	
	private long year;
	
	private String genre;
	private String director;

	public Movie() {

	}

	@Override
	public String toString() {
		return "movie_title=" + movie_title + ", year=" + year + ", director=" + director + ", sequel=" + sequel;
	}

	public String getMovie_title() {
		return movie_title;
	}

	public void setMovie_title(String movie_title) {
		this.movie_title = movie_title;
	}


	public long getYear() {
		
		return year;
	}

	public void setYear(long year) {
		this.year = year;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Movie getSequel() {
		return sequel;
	}

	public void setSequel(Movie sequel) {
		this.sequel = sequel;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre=genre;
	}

	public Set<Cast> getCast() {
		return cast;
	}

	public void setCast(Set<Cast> cast) {
		this.cast = cast;
	}
	
	public void addCast(Cast actor) {
		cast.add(actor);
	}
	
	

}
