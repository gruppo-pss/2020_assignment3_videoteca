package com.videoteca.model;


import java.util.Set;
import javax.persistence.*;

@Entity
public class Copy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long copy_id;


	@ManyToMany
	@JoinTable(
   		  name = "Manage", 
   		  joinColumns = @JoinColumn(name = "copy_id"), 
   		  inverseJoinColumns = @JoinColumn(name = "email"))
	private Set<Admin> admins;

	@ManyToOne
	@JoinColumn(name = "movie_title", nullable=false)
	private Movie movie;
	
	@ManyToOne
	@JoinColumn(name="email", nullable=true)
	private Customer customer;

	public Copy() {
		
	}
	
	public long getId() {
		return copy_id;
	}
	
	public void setId(long copy_id) {
		this.copy_id = copy_id;
	}
	
	public void setMovie(Movie m) {
		this.movie = m;
	}
	
	public Movie getMovie(){
		return movie;
	}
	
	public Customer getCustomer(){
		return customer;
	}
	
	public void setCustomer(Customer c) {
		this.customer = c;
	}
	
	public Set<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<Admin> admins) {
		this.admins = admins;
	}

	@Override
	public String toString() {
		return "Copy [copy_id=" + copy_id + ", admins=" + admins + ", movie=" + movie + "]";
	}
	

}
