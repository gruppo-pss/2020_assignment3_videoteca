package com.videoteca.servicies;


import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.videoteca.model.Admin;
import com.videoteca.model.Copy;
import com.videoteca.model.Movie;
import com.videoteca.repositories.AdminRepository;
import com.videoteca.repositories.CopyRepository;
import com.videoteca.repositories.MovieRepository;

@Service
public class CopyService {
	@Autowired
	private CopyRepository copyRepository;
	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private AdminRepository adminRepository;

	/*
	 * Permette di aggiungere una copia al database, associandola al rispettivo film (già presente nel database)
	 */
	public long addCopy(String movie_title) {
		Copy copy = new Copy();
		
		Optional<Movie> m = movieRepository.findById(movie_title);
		// String message = "Non è stato possibile aggiungere la copia in quanto non esiste alcun film dal titolo " + movie_title; 
		copy.setMovie(m.get());
		copyRepository.save(copy);
		long message =  copy.getId();
		
		return message;
	}
	
	/*
	 * Permette di visualizzare le informazioni relative a tutte le copie memorizzate nel db 
	 */
	public Iterable<Copy> getAllCopies() {
		
		Iterable<Copy> copies = copyRepository.findAll();
		return copies;
	}
	
	/*
	 * Permette di visualizzare le informazioni relative ad una copia, dato il suo id
	 */
	public String getCopyById(long id) {
		Optional<Copy> copy = copyRepository.findCopyById(id);
		if (copy.isPresent()) {
			return copy.get().toString();
		}
		return "Non è presente alcuna copia avente id " + id;
	}
	
	/*
	 * Permette di visualizzare tutte le copie associate ad un customer (ovvero quelle ch eil customer
	 * ha comprato nel corso del tempo), data la sua email
	 */
	public Iterable<Copy> getCopiesByCustomer(String customerMail) {
		Iterable<Copy> copies = copyRepository.getCopiesByCustomer(customerMail);
		return copies;
	}
	
	/*
	 * Permette di associare ad un utente una copia di un film. Ovvero permette di modellare il fatto 
	 * che l'utente ha comprato una copia di un film
	 */
	public void buyCopy(String email, long copy_id) {
		copyRepository.buyCopy(email, copy_id);
	}
	
	/*
	 * Permette di eliminare una copia, solo se essa non è associata ad alcun cliente (ovvero se non è stata venduta in precedenza). 
	 */
	public boolean deleteCopy(long id) {
		Copy copy = copyRepository.findCopyById(id).get();
		if (copy.getCustomer() == null) {
			// per prima cosa elimino la relazione tra la copia e l'admin che l'ha creata, in seguito elimino la copia
			adminRepository.deleteManage(id);
			copyRepository.deleteCopy(id);
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Permette di visualizzare solo le copie disponibili (ovvero quelle che non sono ancora state vendute ad alcun cliente)
	 */
	public Iterable <Copy> getAllAvailaibleCopies() {
		Iterable<Copy> copies = copyRepository.selectAvailableCopies();
		return copies;
	}
	
	/*
	 * Permette di modificare il film associato alla copia. Viene  resa persistente l'informazione relativa all'admin
	 * che ha modificato la copia
	 */
	public void updateCopy(long copy_id, String movieTitle, String adminMail) {
		// per prima cosa viene associata la copia all'admin che l'ha modificata, in seguito si esegue la modifica della copia
		Copy copy = copyRepository.findCopyById(copy_id).get();
		Admin admin = adminRepository.findById(adminMail).get();
		Set<Copy> copies = admin.getCopies();
		if(!copies.contains(copy))
		{
			copies.add(copy);
			admin.setCopies(copies);
			adminRepository.save(admin);
		}
		copyRepository.updateCopy(movieTitle, copy_id);
	}
	
	public Optional<Copy>  findById(long copy_id) {
		return copyRepository.findCopyById(copy_id);
	}
	
}
