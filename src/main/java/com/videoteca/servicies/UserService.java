
package com.videoteca.servicies;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.videoteca.model.Admin;
import com.videoteca.model.Customer;
import com.videoteca.repositories.CustomerRepository;
import com.videoteca.repositories.AdminRepository;


@Service
public class UserService implements UserDetailsService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	AdminRepository adminRepository;

	
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {		
		
		if (!customerRepository.findById(email).isPresent()) {
			
			
				if (!adminRepository.findById(email).isPresent()) 
				{

					throw new UsernameNotFoundException("User not found");
					
				}
				Admin admin = adminRepository.findById(email).get();
				return new org.springframework.security.core.userdetails.User(
						admin.getEmail(),
						admin.getPassword(),
						Collections.singleton(
								new SimpleGrantedAuthority("ADMIN")));
			}
		
		Customer customer = customerRepository.findById(email).get();
		return new org.springframework.security.core.userdetails.User(
				customer.getEmail(),
				customer.getPassword(),
				Collections.singleton(
						new SimpleGrantedAuthority("CUSTOMER")));
				
	}
}

