package com.videoteca.servicies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.videoteca.repositories.CastRepository;
import com.videoteca.repositories.MovieRepository;
import com.videoteca.model.Cast;
import com.videoteca.model.Movie;


@Service
public class MovieService {
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private CastRepository castRepository;
	
	// Metodo per aggiungere nuovi film al db
	public Movie addNewMovie(String title, String genre, String director, long year) {
		Movie movie = new Movie();
		movie.setMovie_title(title);
		movie.setGenre(genre);
		movie.setDirector(director);
		movie.setYear(year);
		
		movieRepository.save(movie);
		
		return movie;
	}
	
	
	// Metodo per aggiungere un sequel a un film già presente
	public String addSequel(String movie_title, String sequel_title) {

			Movie movie = movieRepository.findById(movie_title).get();
			Movie sequel;
			if(movieRepository.existsById(sequel_title)) {
				sequel = movieRepository.findById(sequel_title).get();
			}
			else
				return "Sequel non presente nel db";
			
			movie.setSequel(sequel);

			movieRepository.save(sequel);
			
			return "Sequel of " + movie.getMovie_title() + " added";
	}
	
	// Metodo per modificare un film
	public String updateMovie(Movie movie) {
		Movie m = movieRepository.findById(movie.getMovie_title()).get();
		if(!movie.getSequel().getMovie_title().contains("null"))
		{
			Movie sequel = movieRepository.findById(movie.getSequel().getMovie_title()).get();
			m.setSequel(sequel);
		}
		m.setDirector(movie.getDirector());
		m.setGenre(movie.getGenre());
		m.setYear(movie.getYear());

		movieRepository.save(m);
		
		return "Movie Updated";
	}
	
	public Movie getSequel(String movie_title ){
		 return movieRepository.findById(movie_title).get().getSequel();
	}
	
	
	public Movie getByTitle(String movie_title) {
		if(movieRepository.findById(movie_title).isPresent())
			return movieRepository.findById(movie_title).get();
		return null;
	}
	
	// Metodo da sistemare in futuro
	public Iterable<Movie> getAllMovies() {
		
		Iterable<Movie> movies = movieRepository.findAll();
		
		return movies;
		
	}
	
	public Iterable<Movie> searchMovies(String keyword){
		return movieRepository.findAll();
	}
	
	// Eliminazione del film e la sua presenza come sequel in altri film
	public String deleteMovie(String title) {
		
		Movie dMovie = movieRepository.findById(title).get();
		
		Iterable<Movie> movies = movieRepository.findAll();
		
		for (Movie movie : movies) {
			if(movie.getSequel()!=null && movie.getSequel().getMovie_title().equals(dMovie.getMovie_title())) {
				movie.setSequel(null);
			}
			if(dMovie.getSequel()!=null)
				dMovie.setSequel(null);
		}
		
		movieRepository.deleteById(title);
		
		return "Movie deleted";
	}
	
	public String addCast(String movie_title, Iterable<Cast> cast) {
		
		if(!movieRepository.existsById(movie_title))
			return "Movie doesn't exist";
		
		for (Cast actor : cast) {
			castRepository.setAct(actor.getCast_id(), movie_title);
		}
				
		return "Cast added";
		
	}
	
	public Iterable<Cast> getAllCast(){
		return castRepository.findAll();
	}
	
	public Iterable<Cast> getAllCastForMovie(String movie_title){
		Iterable<Cast> cast = castRepository.FindCast(movie_title);
		return cast;
		
	}
	
	
	
}
