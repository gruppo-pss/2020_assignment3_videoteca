package com.videoteca.servicies;


import java.sql.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;


import com.videoteca.model.Cast;
import com.videoteca.repositories.CastRepository;





@Service
@Transactional
public class CastService {

	@Autowired
	private CastRepository castRepository;
	
	
	public String addNewActor(String name, String surname, Date birthdate)
	{
		 Cast actor = new Cast(name, surname, birthdate);
		 castRepository.save(actor);
		 
		 return "Actor added";
		
	}
	
	
	public ModelAndView FindActorBySurname(String surname)
	{
		ModelAndView mav = new ModelAndView("get_by_surname");
		mav.setViewName("cast");
		Optional<Cast> c = castRepository.FindActorBySurname(surname);
		Cast cast = null;
		try{
			cast = c.get();
		}catch(Exception e) {
			mav.getModel().put("cast", null);
		}
		mav.getModel().put("cast", cast);
		return mav;
		
	}
	
	public void setMovieCast(long id_cast, String movie_title) {
		castRepository.setAct(id_cast, movie_title);
	}
	
	
	// Da usare per la funzione di cerca cast
	public ModelAndView findCast(String id_movie)
	{
		ModelAndView mav = new ModelAndView("cast");
		
		Iterable<Cast> cast = castRepository.FindCast(id_movie);
		
		mav.getModel().put("cast", cast);
		
		// cast = cast.stream().map(actor -> Helper.castFromDTO(new CastDTO(actor))).collect(Collectors.toList());
		
		return mav;
		
	}
	
	
	public Iterable<Cast> FindAll()
	{
		return castRepository.findAll();
	}


	public String deleteActor(long id_cast) {
	
		
		castRepository.deleteById((int) id_cast);
		
		return "Actor deleted";
	}
	
}
