package com.videoteca.servicies;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.videoteca.model.Admin;
import com.videoteca.model.Copy;
import com.videoteca.repositories.AdminRepository;
import com.videoteca.repositories.CopyRepository;

@Service
@Transactional
public class AdminService {
	
	@Autowired
	AdminRepository adminRepository;
	@Autowired
	CopyRepository copyRepository;
	
	public String addNewAdmin(String email, String nickname, String password, String contract) {
		Admin admin = new Admin();
		admin.setEmail(email);
		admin.setNickname(nickname);
		admin.setPassword(password);
		admin.setContract(contract);
		
		adminRepository.save(admin);
		
		return "Admin added";
	}

	
	public Admin GetAdmin(String email) {
		if(!adminRepository.existsById(email))
			return null;
		return  adminRepository.findById(email).get();
		
		
		
	}
	
	public Admin update(String email) {
		
		return adminRepository.findById(email).get();
		
		
	}
	
	public String updateAdmin(Admin admin) {
		if(admin.getPassword() == ""){
			// Se non ha modificato la password, riutilizzo la stessa
			String email = SecurityContextHolder.getContext().getAuthentication().getName();
			Optional<Admin> loggedAdmin = adminRepository.findById(email);
			admin.setPassword(loggedAdmin.get().getPassword());
		}
		else {
			// ha cambiato la pasowrd, l'aggiorno e ne creo l'hash per memorizzarla nel db 
			String hashedPsw = BCrypt.hashpw(admin.getPassword(), BCrypt.gensalt());
			admin.setPassword(hashedPsw);
		}
		adminRepository.updateAdmin(admin.getEmail(), admin.getNickname(), admin.getPassword(), admin.getContract());
		return "redirect:/admin/admin_information/?email=" + admin.getEmail();
	}
	
	public String pairAdminCopy(String email, long idCopy) {
		
		
		Copy copy = copyRepository.findCopyById(idCopy).get();
		Admin admin = adminRepository.findById(email).get();
		Set<Copy> copies = admin.getCopies();
		copies.add(copy);
		admin.setCopies(copies);
		adminRepository.save(admin);
		
		return "Admin "+email+" ha aggiunto una copia";
	}
	
	
	
}

	
	