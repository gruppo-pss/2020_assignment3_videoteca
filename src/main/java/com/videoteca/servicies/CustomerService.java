package com.videoteca.servicies;



import java.sql.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.videoteca.model.Customer;
import com.videoteca.repositories.CustomerRepository;



@Service
@Transactional
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	
	public String addNewCustomer(String email, String nickname, String password, Date registration) {
		Customer customer = new Customer();
		customer.setEmail(email);
		customer.setNickname(nickname);
		customer.setPassword(password);
		customer.setRegistration(registration);
		
		customerRepository.save(customer);
		
		return "Customer added";
	}

	/*
	public ModelAndView findAll() {
		ModelAndView mav = new ModelAndView("all_customers");
		Iterable <Customer> allCustomers = customerRepository.findAll();
		mav.getModel().put("customers", allCustomers);
		return mav;
	}
	*/
	
	public Iterable <Customer> findAll() {
		Iterable <Customer> allCustomers = customerRepository.findAll();
		return allCustomers;
	}

	public Customer getCustomer(String email) {
		if (customerRepository.findById(email).isPresent())
			return customerRepository.findById(email).get();
		return null;
		
	 }
	
	
	public Customer update(String email) {
		
		return customerRepository.findById(email).get();
		
		
	}
	
	 public String updateCustomer(Customer customer) {
		 if(customer.getPassword() == ""){
			 // Se non ha modificato la password, riutilizzo la stessa
			String email = SecurityContextHolder.getContext().getAuthentication().getName();
			Optional<Customer> loggedAdmin = customerRepository.findById(email);
			customer.setPassword(loggedAdmin.get().getPassword());
		}
		else 
		{
			// ha cambiato la pasowrd, l'aggiorno e ne creo l'hash per memorizzarla nel db 
			String hashedPsw = BCrypt.hashpw(customer.getPassword(), BCrypt.gensalt());
			customer.setPassword(hashedPsw);
		}
		customerRepository.updateCustomer(customer.getEmail(), customer.getNickname(), customer.getPassword());
		return "redirect:/customer/customer_information";
	}
	
	
	
	
}
