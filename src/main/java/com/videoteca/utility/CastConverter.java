package com.videoteca.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.videoteca.model.Cast;
import com.videoteca.repositories.CastRepository;


@Component
public class CastConverter implements Converter<String, Cast>{
	
	@Autowired
	private CastRepository castRepository;
	
	@Override
	public Cast convert(String cast_id) {
			
		long id = Long.parseLong(cast_id);
		
		Cast c = castRepository.findByidCast(id).get();

		return c;
	}
	
	
}

