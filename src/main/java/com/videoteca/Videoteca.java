package com.videoteca;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Videoteca {

	public static void main(String[] args) {
		SpringApplication.run(Videoteca.class, args);
	}

}
