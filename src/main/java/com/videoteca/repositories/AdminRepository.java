package com.videoteca.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.videoteca.model.Admin;



public interface AdminRepository extends CrudRepository<Admin, String> {

	@Modifying
    @Query(value = "UPDATE Admin SET nickname = ?2, password = ?3, contract = ?4 WHERE email = ?1", nativeQuery = true)
	public void updateAdmin (String email, String new_nickname, String new_password, String contract);
	
	@Modifying
	@Transactional
    @Query(value = "DELETE from manage WHERE copy_id = ?1", nativeQuery = true)
	public void deleteManage(long copy_id);
	
}
