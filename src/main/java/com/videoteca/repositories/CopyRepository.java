package com.videoteca.repositories;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.videoteca.model.Copy;


public interface CopyRepository extends CrudRepository<Copy, Integer> {

	/*
	 * Permette di prelevare le informazioni relative a tutte le copie che sono state comprate da un determinato customer 
	 */
    @Query(value = "SELECT c.copy_id, c.email, c.movie_title from copy c where c.email = ?1", nativeQuery = true)
	public Iterable<Copy> getCopiesByCustomer(String email);
	
	/*
	 * Permette di associare ad un utente una copia. Ovvero permette di modellare il fatto che l'utente ha 
	 * comprato una copia di un film
	 */
	@Modifying
	@Transactional
    @Query(value = "UPDATE copy c set email = ?1 where c.copy_id = ?2", nativeQuery = true)
	public void buyCopy(String email, long copy_id);
	
	/**
	 * Permette di eliminare una copia 
	 */
	@Modifying
	@Transactional
    @Query(value = "DELETE from copy where copy_id = ?1", nativeQuery = true)
	public void deleteCopy(long copy_id);
	
	/*
	 * Permette di modificare il customer di una copia 
	 */
	@Modifying
	@Transactional
    @Query(value = "UPDATE copy c set c.movie_title = ?1 where c.copy_id = ?2", nativeQuery = true)
	public void updateCopy(String movieTitle, long copy_id);
	
	/**
	 * Permette di trovare tutte le copie non vendute in precedenza (ovvero disponibili per il cliente)
	 */
	@Query(value = "SELECT * from copy where email IS NULL", nativeQuery = true)
	public Iterable<Copy> selectAvailableCopies();
	
	
	@Query(value = "SELECT * FROM copy WHERE copy_id = ?1", nativeQuery = true)
	public Optional<Copy> findCopyById(long id);
}
