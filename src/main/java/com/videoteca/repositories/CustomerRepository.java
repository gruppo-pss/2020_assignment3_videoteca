package com.videoteca.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.videoteca.model.Customer;


public interface CustomerRepository extends CrudRepository<Customer, String>{

	
	@Modifying
    @Query(value = "UPDATE Customer SET nickname = ?2, password = ?3 WHERE email = ?1", nativeQuery = true)
	public void updateCustomer(String email, String new_nickname, String new_password);

	//Set<Customer> findByCopy(Copy copy); // Find all customer by the copy
	
	


}
