package com.videoteca.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.videoteca.model.Cast;

public interface CastRepository extends CrudRepository<Cast, Integer>{

	@Query(value = "SELECT c.cast_id, c.surname, c.name, c.birthdate FROM Cast c where c.surname = ?1", nativeQuery = true)
	public Optional<Cast> FindActorBySurname(String surname);

	@Modifying
	@Transactional
    @Query(value = "INSERT INTO Act (cast_id, movie_title) VALUES (?1, ?2)", nativeQuery = true)
	public void setAct(long cast_id, String movie_title);


	@Query(value = "SELECT c.cast_id, c.name, c.surname, c.birthdate FROM Cast c JOIN Act a on a.cast_id = c.cast_id where a.movie_title = ?1", nativeQuery = true)
	public List<Cast> FindCast(String movie_title);
	
	@Query(value = "SELECT * FROM cast WHERE cast_id = ?1", nativeQuery = true)
	public Optional<Cast> findByidCast(Long id);

}
