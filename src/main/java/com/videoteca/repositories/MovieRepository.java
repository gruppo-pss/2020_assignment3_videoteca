package com.videoteca.repositories;

import org.springframework.data.repository.CrudRepository;
import com.videoteca.model.Movie;

public interface MovieRepository extends CrudRepository<Movie, String>{
	
	
/*
	@Query(value = "SELECT * FROM movie as m WHERE m.movie_title LIKE %?1%" 
			+ "OR m.directo LIKE ?1" 
			+ "OR m.genre LIKE ?1" 
			+ "OR m.year LIKE ?1;", nativeQuery = true)
	public Iterable<Movie> searchAll(String keyword);
*/
}
