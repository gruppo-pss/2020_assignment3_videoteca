package com.videoteca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.Admin;
import com.videoteca.servicies.AdminService;



@Controller // This means that this class is a Controller
@RequestMapping(path="/admin") // This means URL's start with /demo (after Application path)
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	 @PostMapping(path="/add") 
	  public @ResponseBody String addNewAdmin(@RequestParam String email,
			  @RequestParam String nickname, @RequestParam String password, @RequestParam String contract) {
		 
		
		 return adminService.addNewAdmin(email, nickname, password, contract);
		
	 }
	 
 	@GetMapping(path = "/admin_information")
 	public ModelAndView getAdmin() {
 		String email = SecurityContextHolder.getContext().getAuthentication().getName(); // prendo l'email dell'utente attualmente loggato
		Admin admin = adminService.GetAdmin(email);
		ModelAndView mav = new ModelAndView("admin_information");
		mav.getModel().put("admin",admin);
		return mav;
	}
 	
 	@GetMapping(path = "/update_admin")
 	public ModelAndView updateAdmin(@RequestParam String email) {
 		ModelAndView mav = new ModelAndView("update_admin");
		Admin admin = adminService.update(email);
		mav.getModel().put("admin",admin);
		return mav;
	}

 	@PostMapping(path = "/update")
	public String updateAdmin(@ModelAttribute("admin") Admin admin) {
		adminService.updateAdmin(admin);
		return "redirect:/admin/admin_information/?email=" + admin.getEmail();
	}
 	/*
 	@GetMapping(path = "/id_copy/{id}")
 	public String pairCopy(@PathVariable(value = "id") int copyId) {
 		String admin = "giovanni.verdi@gmail.com";
 		adminService.pairAdminCopy(admin, copyId);
 		return "redirect:/movie/get_all";
 	}
 	*/
}
