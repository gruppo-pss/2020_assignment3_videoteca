package com.videoteca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.Cast;
import com.videoteca.servicies.CastService;




@Controller // This means that this class is a Controller
@RequestMapping(path="/cast") // This means URL's start with /demo (after Application path)
public class CastController {

	@Autowired
	private CastService castService;
	
	
	@GetMapping(path = "/all")
	 public ModelAndView getCast(){
			
			ModelAndView mav = new ModelAndView("castList");
			Iterable<Cast> cast = castService.FindAll();
			
			mav.addObject("cast", cast);
			
			
			return mav;
		}
	
	@GetMapping(path = "/add/new_actor")
	public ModelAndView addNewActor() {
		
		ModelAndView mav = new ModelAndView("addNewActor");
		Cast actor = new Cast();
		
		mav.addObject("cast", actor);
		
		return mav;
	}
	
	@PostMapping(path="/add/new_actor")
	public String addNewActor(@ModelAttribute (value = "cast") Cast cast) {
		
		castService.addNewActor(cast.getName(), cast.getSurname(), cast.getBirthdate());
		
		return "redirect:/cast/all";
	}
	
	
	/*
	@PostMapping(path="/add") // Map ONLY POST Requests
	  public @ResponseBody String addNewActor(@RequestParam String name,
			  @RequestParam String surname, @RequestParam String birthdate) {
		 
		castService.addNewActor(name, surname, birthdate);
		 
		 return "Actor added";
	 }
	*/
	
	
	
	
	@PostMapping(path="/save_cast") // Map ONLY POST Requests
	  public @ResponseBody String setMovieCast(@RequestParam int id_cast, @RequestParam String movie_title) {
		 
		castService.setMovieCast(id_cast, movie_title);
		 
		 return "Cast added";
	 }
	
	 
	 
	 @GetMapping("/get_by_surname")
	 public ModelAndView get_by_surname(@RequestParam String surname)
	 {
		return castService.FindActorBySurname(surname);
	 }
	
	 @PostMapping(path = "/delete")
		public @ResponseBody String deleteActor(@RequestParam int id_cast) {
			
			return castService.deleteActor(id_cast);
			
		}
	 

	 
}
