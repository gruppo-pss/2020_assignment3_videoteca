package com.videoteca.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.Customer;
import com.videoteca.servicies.CustomerService;

@Controller // This means that this class is a Controller
@RequestMapping(path="/customer") // This means URL's start with /demo (after Application path)
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@PostMapping(path="/add") // Map ONLY POST Requests
	  public @ResponseBody String addNewCustomer(@RequestParam String email,
			  @RequestParam String nickname, @RequestParam String password, @RequestParam Date registration) {
		 
		return customerService.addNewCustomer(email, nickname, password, registration);
	 }
	 
	 @GetMapping(path = "/all")
	 public ModelAndView getAllCustomer(){
		 ModelAndView mav = new ModelAndView("all_customers");
		 mav.getModel().put("customers", customerService.findAll());
		return mav;
	 }
	 
	 @GetMapping(path = "/customer_information")
	 public ModelAndView getcustomer() {
		String email = SecurityContextHolder.getContext().getAuthentication().getName(); // prendo l'email dell'utente attualmente loggato
		ModelAndView mav = new ModelAndView("customer_information");
		Customer customer = customerService.getCustomer(email);
		mav.getModel().put("customer", customer);
		return mav;
	}
	 	
	@GetMapping(path = "/update_customer")
	public ModelAndView updateCustomer(@RequestParam String email) {	
		ModelAndView mav = new ModelAndView("update_customer");
		Customer customer = customerService.update(email);
		mav.getModel().put("customer",customer);
		return mav;
	}
	 	
	@PostMapping(path = "/update")
	public String updateCustomer(@ModelAttribute("customer") Customer customer) {
		customerService.updateCustomer(customer);
		return "redirect:/customer/customer_information";
	}
			
			
	 	
}

