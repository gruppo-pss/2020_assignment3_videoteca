package com.videoteca.controller;

import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.Cast;
import com.videoteca.model.Movie;
import com.videoteca.servicies.CastService;
import com.videoteca.servicies.MovieService;

@Controller // This means that this class is a Controller
@RequestMapping(path="/movie") // This means URL's start with /demo (after Application path)
public class MovieController {
	
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private CastService castService;
	
	
	
	/*
	 * Mostra tutti i film salvati nel database
	 */
	@GetMapping(path="/get_all") 
	public ModelAndView getAllMovie(@Param("keyword") String keyword) {

		ModelAndView mav = new ModelAndView("movieList");
		Iterable<Movie> movies = movieService.getAllMovies();
		mav.getModel().put("movies", movies);
		
		return mav;
		
	}
	
	
	@GetMapping(path = "/search")
	public ModelAndView getSearch(@Param("keyword") String keyword) {
		
		ModelAndView mav = new ModelAndView("searchResults");
		try{
			Movie movie = movieService.getByTitle(keyword);
			mav.addObject("movie", movie);
		}catch(NoSuchElementException e) {
			Movie  movie = new Movie();
			mav.addObject("movie", movie);
		}
		
		
		
		return mav;
	}

	
	@GetMapping(path="/add")
	public ModelAndView addNewMovie() {
		
		ModelAndView mav = new ModelAndView("addNewMovie");
		
		Movie movie = new Movie();
		mav.addObject("movie", movie);
		// model.addAttribute("movie", movie);	
		
		return mav;
	}
	
	@PostMapping(path = "/add")
	public String submitMovie(@ModelAttribute("movie") Movie movie) {
		
		
		movieService.addNewMovie(movie.getMovie_title(), movie.getGenre(), movie.getDirector(), movie.getYear());
		
		return "redirect:/movie/add_actor/"+movie.getMovie_title();
	}
	
	
	@GetMapping(path="/add_actor/{movie_title}")
	public ModelAndView addActor(@PathVariable(value = "movie_title") String movie_title) {//, String idActor) {
		
		ModelAndView mav = new ModelAndView("addCastToMovie");
		
		Movie movie = movieService.getByTitle(movie_title);
		Iterable<Cast> cast = movieService.getAllCast();
		
		mav.addObject("movie", movie);
		mav.addObject("cast", cast);
		
		
		return mav;
		
	}
	
	@PostMapping(path = "/save_actor")
	public String addActor(@ModelAttribute (value = "movie") Movie movie) {
		
		movieService.addCast(movie.getMovie_title(), movie.getCast());
		
		return "redirect:/movie/get_all";
	}
	
	
	@GetMapping(path="/choose_prequel")
	public ModelAndView choosePrequel() {
		
		ModelAndView mav = new ModelAndView("choosePrequel");
		Iterable<Movie> movies = movieService.getAllMovies(); 
		Movie movie = new Movie();
		
		mav.addObject("movies", movies);
		mav.addObject("movie", movie);
		
		return mav;
	}
	
	@PostMapping(path = "/add/choose_prequel")
	public String choosePrequel(@ModelAttribute(value = "movie") Movie movie) {
		
		movieService.addSequel(movie.getMovie_title(), movie.getSequel().getMovie_title());
		
		return "redirect:/movie/get_all" ; // /movie/add/sequel/"+movie.getMovie_title();
	}
	
	@GetMapping(path = "/delete/{movie_title}")
	public ModelAndView deleteMovie(@PathVariable (value = "movie_title" )String movie_title) {
		
		ModelAndView mav = new ModelAndView("movieList");
		movieService.deleteMovie(movie_title);
		Iterable <Movie> movies = movieService.getAllMovies();
		mav.addObject("movies", movies);
		return mav;	
	}
	
	/*
	@GetMapping(path = "/delete")
	public ModelAndView deleteMovie() {
		
		ModelAndView mav = new ModelAndView("deleteMovies");
		Cast cast = new Cast(); // Utile per memorizzare l'insieme di film da eliminare
		Iterable<Movie> movies = movieService.getAllMovies();
		mav.addObject("movies", movies);
		mav.addObject("cast", cast);
		return mav;	
	}
	*/
	
	@PostMapping(path = "/delete")
	public String deleteMovie(@ModelAttribute(value = "cast") Cast cast) {
		Iterable<Movie> movies = cast.getMovie();
		for (Movie movie : movies) {
			movieService.deleteMovie(movie.getMovie_title());
		}
		return "redirect:/movie/get_all";
		
	}
	
	
	
	
	@GetMapping(path = "/update/{movie_title}")
	public ModelAndView updateMovie(@PathVariable (value = "movie_title" )String movie_title) {
		
		ModelAndView mav = new ModelAndView("updateMovie");
		Movie movie = movieService.getByTitle(movie_title);
		mav.addObject("movie", movie);
		Iterable <Movie> movies = movieService.getAllMovies();
		mav.addObject("movies", movies);
		return mav;
	}
	
	
	@PostMapping(path = "/update")
	public String updateMovie(@ModelAttribute(value = "movie") Movie movie) {
		
		movieService.updateMovie(movie);
		
		return "redirect:/movie/get_all";
		
	}
	
	@GetMapping(path = "/get_cast")
	public @ResponseBody ModelAndView getCast(@RequestParam String movie_title){
		 return castService.findCast(movie_title);
	 }
	
	// Inutilizzato 
		@GetMapping(path = "/sequel/{movie_title}")
		public @ResponseBody Movie getSequel(@PathVariable (value = "movie_title" ) String movie_title){
			 return movieService.getSequel(movie_title);
		 }
	
	
	
	
	
	
	
	
	
	
	
}
