package com.videoteca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.Admin;
import com.videoteca.model.Customer;
import com.videoteca.servicies.AdminService;
import com.videoteca.servicies.CustomerService;
import com.videoteca.servicies.UserService;


@Controller
public class LoginController {
	
	@Autowired
	AdminService adminService;
	@Autowired
	CustomerService customerService;
	@Autowired 
	UserService userService;
	
    @GetMapping("/login")
    public ModelAndView login() {
    	ModelAndView mav = new ModelAndView("login");
        return mav;
    }
    
    @RequestMapping(value = { "", "/", "/index" })
	public ModelAndView index(Model model, @RequestParam(value="error", required=false) String param) {
		
		if(param != null) {
			ModelAndView mav = new ModelAndView("error");
			mav.getModel().put("message", "Wrong username or password");
			return mav;
		}
		
		
		
		String role = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString();
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		

		if (role.contains("ADMIN")) {

		    Admin adm = adminService.GetAdmin(email);
			ModelAndView mav = new ModelAndView("indexAdmin");
			mav.getModel().put("name", adm.getNickname());
			return mav;
		    
			
		} 
	    Customer cus = customerService.getCustomer(email);
		ModelAndView mav = new ModelAndView("indexCustomer");
		mav.getModel().put("name", cus.getNickname());
		return mav;
		
	}
	
	
	
	@GetMapping("/index_customer")
	public ModelAndView indexCustomer() {
		ModelAndView mav = new ModelAndView("indexCustomer");
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Customer cus = customerService.getCustomer(email);
		mav.getModel().put("name", cus.getNickname());
		return mav;
	}
	
	@GetMapping("/index_admin")
	public ModelAndView indexAdmin() {
		ModelAndView mav = new ModelAndView("indexAdmin");
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		Admin adm = adminService.GetAdmin(email);
		mav.getModel().put("name", adm.getNickname());
		return mav;
	}
	

}
