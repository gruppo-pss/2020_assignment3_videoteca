package com.videoteca.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.videoteca.model.*;
import com.videoteca.servicies.AdminService;
import com.videoteca.servicies.CopyService;
import com.videoteca.servicies.CustomerService;
import com.videoteca.servicies.MovieService;


@Controller 
@RequestMapping(path="/copy") 
public class CopyController {
	@Autowired
	private CopyService copyService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private MovieService movieService;
	
	@GetMapping(path="/add") 
	public String addCopy(@RequestParam String movie_title) {
		
		long idCopy = copyService.addCopy(movie_title);
		// String admin = "giovanni.verdi@gmail.com";
		String admin = SecurityContextHolder.getContext().getAuthentication().getName();
 		adminService.pairAdminCopy(admin, idCopy);
		return  "redirect:/movie/get_all";//"redirect:/admin/id_copy/"+idCopy;
		
	}
	
	@GetMapping(path="/get_all") 
	public ModelAndView getAllCopies() {
		ModelAndView mav = new ModelAndView("all_copies");
		Iterable<Copy> copies =  copyService.getAllCopies();
		mav.getModel().put("copies", copies);
		return mav;
	}
	
	@ResponseBody
	@GetMapping(path="/get_by_id") 
	public String getCopyById(@RequestParam long id) {
		return copyService.getCopyById(id);
	}
	
	@GetMapping(path="/get_by_customer") 
	public ModelAndView getCopiesByCustomer() {
		ModelAndView mav = new ModelAndView("copiesByCustomer");
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		// String email = "l.tomasoni4@campus.unimib.it";  // Questo andrà sostituito con il dato del cookie dell'utente loggato
		Iterable <Copy> copies = copyService.getCopiesByCustomer(email);
		mav.getModel().put("copies", copies);
		return mav;
	}
	
	@ResponseBody
	@GetMapping(path = "/buy")
	public ModelAndView buyCopy(@RequestParam long copy_id) {
		ModelAndView mav = new ModelAndView("copiesByCustomer");
		String customerEmail = SecurityContextHolder.getContext().getAuthentication().getName();
		// String customerEmail = "l.tomasoni4@campus.unimib.it";  // Questo andrà sostituito con il dato del cookie dell'utente loggato
		copyService.buyCopy(customerEmail, copy_id);
		Iterable <Copy> copies = copyService.getCopiesByCustomer(customerEmail);
		mav.getModel().put("copies", copies);
		return mav;	
	}
	
	
	@ResponseBody
	@GetMapping(path = "/update")
	public ModelAndView updateCopy(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("update_copy");
		Copy copy = copyService.findById(id).get();
		mav.getModel().put("copy",copy);
		Iterable<Movie> allMovies = movieService.getAllMovies();
		mav.getModel().put("movies", allMovies);
		return mav;	
	}
	@PostMapping(path = "/update")
	public String updateCopy(@RequestParam long copy_id, @RequestParam String movie) {
		String email = SecurityContextHolder.getContext().getAuthentication().getName(); //l'admin che fa la richiesta
		copyService.updateCopy(copy_id, movie, email);
		return "redirect:/copy/get_all";
	}
	
	@ResponseBody
	@GetMapping(path="/delete") 
	public ModelAndView deleteCopyById(@RequestParam long id) {
		ModelAndView mav = new ModelAndView();
		boolean success = copyService.deleteCopy(id);
		if(success) {
			mav.setViewName("all_copies");
			Iterable<Copy> copies =  copyService.getAllCopies();
			mav.getModel().put("copies", copies);
			return mav;
		}
		mav.setViewName("error");
		mav.getModel().put("message", "Non è stato possibile eliminare la copia numero " + id);
		return mav;
	}
	
	@GetMapping(path="/get_available_copies") 
	public ModelAndView availableCopies() {
		ModelAndView mav = new ModelAndView("buyCopy");
		Iterable <Copy> copies = copyService.getAllAvailaibleCopies();
		mav.getModel().put("copies", copies);
		return mav;
	}
}