package com.videoteca.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.videoteca.servicies.UserService;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	  @Autowired
	  UserService userService;

	String[] resources = new String[]{
            "/include/**",
            "/css/**",
            "/icons/**",
            "/img/**",
            "/js/**",
            "/static/**",
            "/layer/**"
            
    };

	
	
	 	@Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	        	.csrf().disable()
	            .authorizeRequests()
		        .antMatchers(resources).permitAll()  
		        .antMatchers("/login").permitAll()
		        .antMatchers("/index").permitAll()
		        .antMatchers(
		        		"/index_admin/",
		        		"/admin/",
		        		"/cast/",
		        		"/copy/",
		        		"/movie/",
		        		"/cast/",
		        		"/customer/").access("hasRole('ADMIN')")
		        
		        
		        .antMatchers(
		        		"/index_customer/",
		        		"/customer/customer_information/",
		        		"/customer/update/",
		        		"customer/update_customer/",
		        		"/movie/get_cast/",
		        		"/copy/get_by_customer/",
		        		"/copy/buy/",
		        		"/copy/get_available_copies/").access("hasRole('CUSTOMER')")
	                .anyRequest().authenticated()
	              
	             .and()
	            .formLogin()
	                .loginPage("/login")
	                .permitAll()
	                .loginProcessingUrl("/signIn")
	                .defaultSuccessUrl("/index")
	                .failureUrl("/index?error=true")
	                .usernameParameter("email")
	                .passwordParameter("password")
	                .and()
	            .logout()
	                .permitAll()
	                .logoutUrl("/logout")
	                .logoutSuccessUrl("/login?logout");
	    }
	    

	 	 BCryptPasswordEncoder bCryptPasswordEncoder;
	    
	     
	     @Bean
	     public BCryptPasswordEncoder passwordEncoder() {
	 		 bCryptPasswordEncoder = new BCryptPasswordEncoder(5);
	         return bCryptPasswordEncoder;
	     }
	 	
	   
	 	
	    
	     @Autowired
	     public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
	  
	         
	         auth.userDetailsService(userService).passwordEncoder(passwordEncoder());     
	         
	         
	         
	     }
	     

	
}
