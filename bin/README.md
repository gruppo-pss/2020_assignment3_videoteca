# 2020_assignment3_videoteca

Terzo Assignemnt del corso "progetto e sviluppo Software".

### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587 @M-Marchi
- Stoppa Miguel 820506 @mig1214
- Tomasoni Lorenzo 829906 @lTomasoni


### Videoteca

“Videoteca” è una semplice applicazione web che consente di gestire l’acquisto di film da parte degli utenti iscritti, dando la possibilità ad essi di visualizzare la lista di film comprati e di acquistarne di nuovi. È inoltre possibile visualizzare le informazioni di ogni film, come ad esempio: il genere, il regista, l’anno di pubblicazione, il cast o l’eventuale presenza di un sequel. 

L’applicazione sarà gestita da un secondo utente, identificato come “Admin” che avrà la possibilità di monitorare i film e gestendo le operazioni CRUD sulle varie tabelle del sistema. 


### Realizzazione

L’applicazione è stata ideata usando una Maven e Spring Application, utilizzando il pattern MVC e data persistency.  Per il servizio di mapping viene usando il middleware Hibernate, che consente di utilizzare un servizio di Object-relational mapping.  
Nella prima fase del assignment ci si è concentrati sul lato back-end, modellando e creando il database e successivamente effettuare un mapping nel modello. I modelli sono gestiti da dei controller, ognuno suddiviso per ogni classe, che ne gestiscono le operazioni CRUD e le viste, da implementare nella seconda fase. Il database è stato creato utilizzando il motore MySQL. 

Il lavoro è stato suddiviso dai membri del gruppo in questo modo:
- Studio collettivo delle tecnologie applicative;
- Ideazione ER del database (Marchi, Tomasoni);
- Creazione Database mySQL (Stoppa);
- Mapping collettivo;
- Creazione dei controller associati alle classi mappate (Stoppa, Tomasoni)
- Documentazione (Marchi)



